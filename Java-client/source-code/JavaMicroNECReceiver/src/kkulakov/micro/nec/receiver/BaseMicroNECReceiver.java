package kkulakov.micro.nec.receiver;

import ch.ntb.usb.Device;
import ch.ntb.usb.USB;
import ch.ntb.usb.USBException;

public class BaseMicroNECReceiver implements Runnable {

	private Device mDevice;
	private boolean didNotifyDisConnection = false;
	
	public BaseMicroNECReceiver(){
		mDevice = USB.getDevice((short) 0x16c0, (short) 0x05dc);
		new Thread(this).run();
	}

	@Override
	public void run() {
		while(true){
			try { Thread.sleep(300); } catch (Exception g){}
			poolDevice();
		}
	}
	
	private void poolDevice(){
		try {
			if (!mDevice.isOpen()){
				mDevice.open(1, 0, -1);
				this.connected();
				didNotifyDisConnection = false;
			}

			byte[] buffer = new byte[32];
			mDevice.controlMsg(192, 2, 0, 0, buffer, 32, 5000, false);
			
			
			if (!new String(buffer).contains("NO")){
				received(buffer[0] == 1, buffer[1] == 1);
				try { Thread.sleep(1500); } catch (Exception g){}
			}
			
		} catch (Exception k) {
			if(!didNotifyDisConnection){
				didNotifyDisConnection = true;
				try { mDevice.close(); } catch (USBException e) {}
				this.disconnected();
			}
		}
	}
	
	/**
	 * Called when new data received.
	 * @param a - MSB
	 * @param b - LSB
	 */
	protected void received(boolean a,boolean b){
		// Nothing should be done on the base class
	}
	
	/**
	 * Called when the receiver connected to USB.
	 * and the device is able to responding to request send to it.  
	 */
	protected void connected(){
		// Nothing should be done on the base class
	}
	
	/** 
	 * Called when the receiver disconnected from the USB
	 * or the device is no longer responding to request sent to it.
	 */
	protected void disconnected(){
		// Nothing should be done on the base class
	}
	
}
