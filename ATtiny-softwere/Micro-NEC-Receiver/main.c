/*
 *  ATtiny2313 - MicroNEC Reciver & usb slave
 *
 *  Created: 02/03/2013
 *  Author: Kirill Kulakov
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>
#include <stdlib.h>


#include "usbdrv/usbdrv.h"

#define F_CPU 12000000L
#include <util/delay.h>

#define USART_BAUDRATE 9600
#define BAUD_PRESCALE (((F_CPU / (USART_BAUDRATE * 16UL))) - 1)

#define USB_LED_OFF 0
#define USB_LED_ON  1
#define USB_DATA_OUT 2

volatile uchar timing[8];

static uchar no_data[8] = "NO";

void decoder(uint16_t *pls);

USB_PUBLIC uchar usbFunctionSetup(uchar data[8]) {
	usbRequest_t *rq = (void *)data; // cast data to correct type
	
	switch(rq->bRequest){
		case USB_DATA_OUT:
			if(timing[2] == 1){
				timing[2] = 0;
				usbMsgPtr = timing;
			} else {
				usbMsgPtr = no_data;
			}				
			return 8;
	}

	
	return 0;
}

int main() {	
	uchar i;
	
	DDRB &= ~(1 << PINB0); //output
	PORTB |= 1 << PB0; //pull up

	wdt_enable(WDTO_1S); // enable 1s watchdog timer

	usbInit();
	
	usbDeviceDisconnect(); // enforce re-enumeration
	for(i = 0; i<250; i++) { // wait 500 ms
		wdt_reset(); // keep the watchdog happy
		_delay_ms(2);
	}
	usbDeviceConnect();
	
	sei(); // Enable interrupts after re-enumeration

	while(1) {
		wdt_reset();
		
		uint16_t highpulse,lowpulse,total = 0;
		uint16_t pulses[4];
		uint8_t currentpulse = 0,flag = 0;
		
		while(1){
			highpulse = lowpulse = 0;
			
			while (PINB & (1<<PINB0)) {
				highpulse++; total++;
				_delay_us(10);
				if(total > 2500)
					goto USB_is_about_to_timeout;
			}

			while (!(PINB & (1<<PINB0))) {
				lowpulse++; total++;
				_delay_us(10);
				if(total > 2500)
					goto USB_is_about_to_timeout;
			}
			
			if(  lowpulse > 270 && lowpulse < 330  ) {
				flag = 1;
				currentpulse=0;
				continue;
			}

			if(  highpulse > 75 || highpulse < 35 )
				flag = 0; //not valid
			
			pulses[currentpulse] = lowpulse;
			
			currentpulse++;
			
			if(currentpulse == 4){
				if( flag == 1)
					decoder(pulses);
				currentpulse=0;
				flag = 0;
			}
		}
		
		USB_is_about_to_timeout:
			
		wdt_reset(); // keep the watchdog happy
		usbPoll();
	}

	return 0;
}


void decoder(uint16_t *pls){
	timing[2] = 1;
	
	if( pls[0] > 30 && pls[0] < 75 && pls[2] > 75 && pls[2] < 125)
		timing[0] = 0;
	else if( pls[2] > 30 && pls[2] < 75 && pls[0] > 75 && pls[0] < 125)
		timing[0] = 1;
	else
		timing[2] = 0; //not valid
		
	if( pls[1] > 30 && pls[1] < 75 && pls[3] > 75 && pls[3] < 125)
		timing[1] = 0;
	else if( pls[3] > 30 && pls[3] < 75 && pls[1] > 75 && pls[1] < 125)
		timing[1] = 1;
	else
		timing[2] = 0; //not valid	
	
}