/*
 *  ATtiny13A - MicroNEC transmitter
 *
 *  Created: 02/03/2013
 *  Author: Kirill Kulakov
 */ 

#include <util/delay.h>
#include <avr/sleep.h>

#define F_CPU 9600000UL
#define BOOL unsigned char

#include <avr/io.h>

int main(void){
	// Define PORTB 4 as an output
	DDRB = 1<<DDB4;
	
	//Send the command 50 times, should be about 0.5 second
	for (int i = 0 ; i < 50 ; i++)
		send(0,0);
	
	// Put MCU into sleep mode
	set_sleep_mode(SLEEP_MODE_PWR_DOWN);
	sleep_mode();
}

void send(BOOL a,BOOL b){
	osc36k(90); //3ms
	
	_delay_ms(0.6);
	if(a)
		osc36k(30);
	else
		osc36k(15);
		
	_delay_ms(0.6);
	if(b)
		osc36k(30);
	else
		osc36k(15);
		
	_delay_ms(0.6);
	if(!a)
		osc36k(30);
	else
		osc36k(15);
	
	_delay_ms(0.6);
	if(!b)
		osc36k(30);
	else
		osc36k(15);
	
	_delay_ms(0.6);
}

void osc36k(int pules){
	for (int i = 0 ; i < pules ; i++){
		PORTB |= (1 << PORTB4);
		_delay_us(14);
		PORTB &= ~(1 << PORTB4);
		_delay_us(14);
	}			
}